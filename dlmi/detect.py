import json
import pathlib

import imageio
import numpy as np
import torch
import torchvision


class DataBrainMRI(torch.utils.data.Dataset):

    def __init__(self, directory, subset, transform=None):
        super().__init__()
        filename = pathlib.Path(directory) / f'{subset}/annotations_{subset}.json'
        self._filename = filename
        with filename.open() as f:
            self._annotations = list(json.load(f).values())
        self._transform = transform

    def __len__(self):
        return len(self._annotations)

    def __getitem__(self, index):
        annotation = self._annotations[index]

        filename = self._filename.parent / annotation['filename']
        image = imageio.imread(filename)
        image = image.astype(np.float32) / 255
        image = image.transpose((2,0,1))

        boxes = []
        for region in annotation['regions']:
            xs = region['shape_attributes']['all_points_x']
            ys = region['shape_attributes']['all_points_y']
            boxes.append([min(xs), min(ys), max(xs), max(ys)])

        areas = [(xmax - xmin) * (ymax - ymin) for xmin, ymin, xmax, ymax in boxes]

        sample = {}
        sample['image'] = image
        sample['boxes'] = np.array(boxes, dtype=np.float32)
        sample['labels'] = np.ones(len(boxes), dtype=np.int64)
        sample['image_id'] = np.array([index], dtype=np.int64)
        sample['area'] = np.array(areas, dtype=np.float32)
        sample['iscrowd'] = np.zeros(len(boxes), dtype=np.int64)
        if self._transform is not None:
            sample = self._transform(sample)
        for key, value in sample.items():
            sample[key] = torch.as_tensor(value)
        return sample


def faster_rcnn(nclasses=2):
    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True, progress=False)
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    model.roi_heads.box_predictor = torchvision.models.detection.faster_rcnn.FastRCNNPredictor(in_features, nclasses)
    return model

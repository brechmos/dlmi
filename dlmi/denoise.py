"""
Functions for CNN denoising using the FFDNet.

Author: Alex Sisniega (asisniega@jhu.edu)

Including code by Matias Tassano with the following license:

Copyright (C) 2018, Matias Tassano <matias.tassano@parisdescartes.fr>

This program is free software: you can use, modify and/or
redistribute it under the terms of the GNU General Public
License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later
version. You should have received a copy of this license along
this program. If not, see <http://www.gnu.org/licenses/>.
"""

import pathlib

import imageio
import numpy as np
import PIL
import torch


class DataChestXRayNoise(torch.utils.data.Dataset):

    def __init__(self, directory, subset, transform=None):
        super().__init__()
        directory = pathlib.Path(directory) / subset
        self._filenames = sorted(pathlib.Path(directory).glob('*.png'))
        self._subset = subset
        self._transform = transform

    def __len__(self):
        return len(self._filenames)

    def __getitem__(self, index):
        filename = self._filenames[index]
        image = imageio.imread(filename).astype(np.float32)
        if self._subset == 'val':
            np.random.seed(seed=index)
            noisy = image + np.random.normal(0, 20, image.shape)
            np.random.seed(seed=None)
        elif self._subset == 'test':
            np.random.seed(seed=index + 1000)
            noisy = image + np.random.normal(0, 20, image.shape)
            np.random.seed(seed=None)
        else:
            noisy = image + torch.FloatTensor(*image.shape).normal_(0, 20).numpy()
        noisy = np.clip(noisy, 0, 255) / 255
        noisy = noisy.astype(np.float32)
        noisy = np.expand_dims(noisy, axis=0)

        image = PIL.Image.fromarray(image.astype(np.uint8))
        image = image.resize((512,512), PIL.Image.BILINEAR)
        image = np.asarray(image, dtype=np.float32) / 255
        image = np.expand_dims(image, axis=0)

        sample = {}
        sample['image'] = noisy
        sample['truth'] = image
        if self._transform is not None:
            sample = self._transform(sample)
        for key, value in sample.items():
            sample[key] = torch.as_tensor(value)
        return sample


# Network models
class UpSampleFeatures(torch.nn.Module):
    r"""Implements the last layer of FFDNet
    """
    def __init__(self):
        super(UpSampleFeatures, self).__init__()

    def forward(self, x):
        return upsamplefeatures(x)


class IntermediateDnCNN(torch.nn.Module):
    r"""Implements the middel part of the FFDNet architecture, which
    is basically a DnCNN net
    """
    def __init__(self, input_features, middle_features, num_conv_layers):
        super(IntermediateDnCNN, self).__init__()
        self.kernel_size = 3
        self.padding = 1
        self.input_features = input_features
        self.num_conv_layers = num_conv_layers
        self.middle_features = middle_features
        if self.input_features == 5:
            self.output_features = 4  # grayscale image
        elif self.input_features == 15:
            self.output_features = 12  # RGB image
        else:
            raise Exception('Invalid number of input features')

        layers = []
        layers.append(torch.nn.Conv2d(in_channels=self.input_features,
                                      out_channels=self.middle_features,
                                      kernel_size=self.kernel_size,
                                      padding=self.padding,
                                      bias=False))
        layers.append(torch.nn.ReLU(inplace=True))
        for _ in range(self.num_conv_layers-2):
            layers.append(torch.nn.Conv2d(in_channels=self.middle_features,
                                          out_channels=self.middle_features,
                                          kernel_size=self.kernel_size,
                                          padding=self.padding,
                                          bias=False))
            layers.append(torch.nn.BatchNorm2d(self.middle_features))
            layers.append(torch.nn.ReLU(inplace=True))
        layers.append(torch.nn.Conv2d(in_channels=self.middle_features,
                                      out_channels=self.output_features,
                                      kernel_size=self.kernel_size,
                                      padding=self.padding,
                                      bias=False))
        self.itermediate_dncnn = torch.nn.Sequential(*layers)

    def forward(self, x):
        out = self.itermediate_dncnn(x)
        return out


class FFDNet(torch.nn.Module):
    r"""Implements the FFDNet architecture
    """
    def __init__(self, num_input_channels):
        super(FFDNet, self).__init__()
        self.num_input_channels = num_input_channels
        if self.num_input_channels == 1:
            # Grayscale image
            self.num_feature_maps = 64
            self.num_conv_layers = 15
            self.downsampled_channels = 5
            self.output_features = 4
        elif self.num_input_channels == 3:
            # RGB image
            self.num_feature_maps = 96
            self.num_conv_layers = 12
            self.downsampled_channels = 15
            self.output_features = 12
        else:
            raise Exception('Invalid number of input features')

        self.intermediate_dncnn = IntermediateDnCNN(
                input_features=self.downsampled_channels,
                middle_features=self.num_feature_maps,
                num_conv_layers=self.num_conv_layers)
        self.upsamplefeatures = UpSampleFeatures()

    def forward(self, x, noise_sigma):
        concat_noise_x = concatenate_input_noise_map(
                x.data, noise_sigma.data)
        concat_noise_x = torch.autograd.Variable(concat_noise_x)
        h_dncnn = self.intermediate_dncnn(concat_noise_x)
        pred_noise = self.upsamplefeatures(h_dncnn)
        return pred_noise


# Aux classes and functions
def concatenate_input_noise_map(input, noise_sigma):
    r"""Implements the first layer of FFDNet. This function returns a
    torch.autograd.Variable composed of the concatenation of the downsampled
    input image and the noise map. Each image of the batch of size CxHxW gets
    converted to an array of size 4*CxH/2xW/2. Each of the pixels of the
    non-overlapped 2x2 patches of the input image are placed in the new array
    along the first dimension.

    Args:
        input: batch containing CxHxW images
        noise_sigma: the value of the pixels of the CxH/2xW/2 noise map
    """
    # noise_sigma is a list of length batch_size
    N, C, H, W = input.size()
    dtype = input.type()
    sca = 2
    sca2 = sca*sca
    Cout = sca2*C
    Hout = H//sca
    Wout = W//sca
    idxL = [[0, 0], [0, 1], [1, 0], [1, 1]]

    # Fill the downsampled image with zeros
    if 'cuda' in dtype:
        downsampledfeatures = torch.cuda.FloatTensor(N, Cout, Hout, Wout).fill_(0)
    else:
        downsampledfeatures = torch.FloatTensor(N, Cout, Hout, Wout).fill_(0)

    # Build the CxH/2xW/2 noise map
    noise_map = noise_sigma.view(N, 1, 1, 1).repeat(1, C, Hout, Wout)

    # Populate output
    for idx in range(sca2):
        downsampledfeatures[:, idx:Cout:sca2, :, :] = \
            input[:, :, idxL[idx][0]::sca, idxL[idx][1]::sca]

    # concatenate de-interleaved mosaic with noise map
    return torch.cat((noise_map, downsampledfeatures), 1)


class UpSampleFeaturesFunction(torch.autograd.Function):
    r"""Extends PyTorch's modules by implementing a torch.autograd.Function.
    This class implements the forward and backward methods of the last layer
    of FFDNet. It basically performs the inverse of
    concatenate_input_noise_map(): it converts each of the images of a
    batch of size CxH/2xW/2 to images of size C/4xHxW
    """
    @staticmethod
    def forward(ctx, input):
        N, Cin, Hin, Win = input.size()
        dtype = input.type()
        sca = 2
        sca2 = sca*sca
        Cout = Cin//sca2
        Hout = Hin*sca
        Wout = Win*sca
        idxL = [[0, 0], [0, 1], [1, 0], [1, 1]]

        assert (Cin % sca2 == 0), \
            'Invalid input dimensions: number of channels should be divisible by 4'

        result = torch.zeros((N, Cout, Hout, Wout)).type(dtype)
        for idx in range(sca2):
            result[:, :, idxL[idx][0]::sca, idxL[idx][1]::sca] = \
                input[:, idx:Cin:sca2, :, :]

        return result

    @staticmethod
    def backward(ctx, grad_output):
        N, Cg_out, Hg_out, Wg_out = grad_output.size()
        dtype = grad_output.data.type()
        sca = 2
        sca2 = sca*sca
        Cg_in = sca2*Cg_out
        Hg_in = Hg_out//sca
        Wg_in = Wg_out//sca
        idxL = [[0, 0], [0, 1], [1, 0], [1, 1]]

        # Build output
        grad_input = torch.zeros((N, Cg_in, Hg_in, Wg_in)).type(dtype)
        # Populate output
        for idx in range(sca2):
            grad_input[:, idx:Cg_in:sca2, :, :] = \
                grad_output.data[:, :, idxL[idx][0]::sca, idxL[idx][1]::sca]

        return torch.autograd.Variable(grad_input)


def weights_init_kaiming(lyr):
    r"""Initializes weights of the model according to the "He" initialization
    method described in "Delving deep into rectifiers: Surpassing human-level
    performance on ImageNet classification" - He, K. et al. (2015), using a
    normal distribution.
    This function is to be called by the torch.nn.Module.apply() method,
    which applies weights_init_kaiming() to every layer of the model.
    """
    classname = lyr.__class__.__name__
    if classname.find('Conv') != -1:
        torch.nn.init.kaiming_normal_(lyr.weight.data, a=0, mode='fan_in')
    elif classname.find('Linear') != -1:
        torch.nn.init.kaiming_normal_(lyr.weight.data, a=0, mode='fan_in')
    elif classname.find('BatchNorm') != -1:
        lyr.weight.data.normal_(mean=0, std=np.sqrt(2./9./64.)).\
            clamp_(-0.025, 0.025)
        torch.nn.init.constant_(lyr.bias.data, 0.0)


# Alias functions
upsamplefeatures = UpSampleFeaturesFunction.apply

import pathlib

import imageio
import numpy as np
import PIL
import torch


class DataRetinaMicroscope(torch.utils.data.Dataset):

    def __init__(self, directory, subset, simulated=False, transform=None):
        ffixeds = sorted(pathlib.Path(directory).glob('images/*_1.jpg'))
        fmovings = sorted(pathlib.Path(directory).glob('images/*_2.jpg'))
        nn = len(ffixeds)
        n = int(round(nn * 0.1))
        splits = torch.utils.data.random_split(range(nn), [nn - 2 * n, n, n])
        indices = splits[('train','val','test').index(subset)].indices
        self._ffixeds = [ffixeds[x] for x in indices]
        self._fmovings = [fmovings[x] for x in indices]
        self._simulated = simulated
        self._subset = subset
        self._transform = transform

    def __len__(self):
        return len(self._ffixeds)

    def __getitem__(self, index):
        crop = 70

        ffixed = self._ffixeds[index]
        fixed = imageio.imread(ffixed)
        fixed = PIL.Image.fromarray(fixed.astype(np.uint8))
        fixed = fixed.convert('RGB')

        if self._simulated:
            if self._subset == 'val':
                np.random.seed(seed=index)
                tx = np.random.uniform(-5, 5)
                ty = np.random.uniform(-5, 5)
                rz = np.random.uniform(-5, 5)
                np.random.seed(seed=None)
            elif self._subset == 'test':
                np.random.seed(seed=index + 100)
                tx = np.random.uniform(-5, 5)
                ty = np.random.uniform(-5, 5)
                rz = np.random.uniform(-5, 5)
                np.random.seed(seed=None)
            else:
                tx = torch.FloatTensor(1).uniform_(-5, 5).item()
                ty = torch.FloatTensor(1).uniform_(-5, 5).item()
                rz = torch.FloatTensor(1).uniform_(-5, 5).item()
            moving = fixed.rotate(rz, resample=PIL.Image.BILINEAR)
            moving = moving.transform(moving.size, PIL.Image.AFFINE, (1,0,tx,0,1,ty))

        fixed = fixed.resize((256+crop*2,256+crop*2), PIL.Image.BILINEAR)
        fixed = np.asarray(fixed, dtype=np.float32) / 255
        fixed = fixed[crop:-crop,crop:-crop]
        fixed = fixed.transpose((2,0,1))

        if not self._simulated:
            fmoving = self._fmovings[index]
            moving = imageio.imread(fmoving)
            moving = PIL.Image.fromarray(moving.astype(np.uint8))
            moving = moving.convert('RGB')

        moving = moving.resize((256+crop*2,256+crop*2), PIL.Image.BILINEAR)
        moving = np.asarray(moving, dtype=np.float32) / 255
        moving = moving[crop:-crop,crop:-crop]
        moving = moving.transpose((2,0,1))

        sample = {}
        sample['fixed'] = fixed
        sample['moving'] = moving
        if self._transform is not None:
            sample = self._transform(sample)
        for key, value in sample.items():
            sample[key] = torch.as_tensor(value)
        return sample


class SpatialTransform(torch.nn.Module):

    def __init__(self):
        super().__init__()

    def meshgrid(self, height, width):
        x_t = torch.matmul(torch.ones([height, 1]), torch.transpose(torch.unsqueeze(torch.linspace(0.0, width - 1.0, width), 1), 1, 0))
        y_t = torch.matmul(torch.unsqueeze(torch.linspace(0.0, height - 1.0, height), 1), torch.ones([1, width]))
        x_t = x_t.expand([height, width]).cuda()
        y_t = y_t.expand([height, width]).cuda()
        return x_t, y_t

    def repeat(self, x, n_repeats):
        rep = torch.transpose(torch.unsqueeze(torch.ones(n_repeats), 1), 1, 0)
        rep = rep.long()
        x = torch.matmul(torch.reshape(x, (-1, 1)), rep).cuda()
        return torch.squeeze(torch.reshape(x, (-1, 1)))

    def interpolate(self, im, x, y):
        im = torch.nn.functional.pad(im, (0,0,1,1,1,1,0,0))
        batch_size, height, width, channels = im.shape
        batch_size, out_height, out_width = x.shape

        x = x.reshape(1, -1)
        y = y.reshape(1, -1)

        x = x + 1
        y = y + 1

        max_x = width - 1
        max_y = height - 1

        x0 = torch.floor(x).long()
        x1 = x0 + 1
        y0 = torch.floor(y).long()
        y1 = y0 + 1

        x0 = torch.clamp(x0, 0, max_x)
        x1 = torch.clamp(x1, 0, max_x)
        y0 = torch.clamp(y0, 0, max_y)
        y1 = torch.clamp(y1, 0, max_y)

        dim2 = width
        dim1 = width*height
        base = self.repeat(torch.arange(0, batch_size)*dim1, out_height*out_width)

        base_y0 = base + y0*dim2
        base_y1 = base + y1*dim2

        idx_a = base_y0 + x0
        idx_b = base_y1 + x0
        idx_c = base_y0 + x1
        idx_d = base_y1 + x1

        # use indices to lookup pixels in the flat image and restore
        # channels dim
        im_flat = torch.reshape(im, [-1, channels])
        im_flat = im_flat.float()
        dim, _ = idx_a.transpose(1,0).shape
        imga = torch.gather(im_flat, 0, idx_a.transpose(1,0).expand(dim, channels))
        imgb = torch.gather(im_flat, 0, idx_b.transpose(1,0).expand(dim, channels))
        imgc = torch.gather(im_flat, 0, idx_c.transpose(1,0).expand(dim, channels))
        imgd = torch.gather(im_flat, 0, idx_d.transpose(1,0).expand(dim, channels))

        # and finally calculate interpolated values
        x1_f = x1.float()
        y1_f = y1.float()

        dx = x1_f - x
        dy = y1_f - y

        wa = (dx * dy).transpose(1,0)
        wb = (dx * (1-dy)).transpose(1,0)
        wc = ((1-dx) * dy).transpose(1,0)
        wd = ((1-dx) * (1-dy)).transpose(1,0)

        output = torch.sum(torch.squeeze(torch.stack([wa*imga, wb*imgb, wc*imgc, wd*imgd], dim=1)), 1)
        output = torch.reshape(output, [-1, out_height, out_width, channels])
        return output

    def forward(self, moving_image, deformation_matrix):
        dx = deformation_matrix[:, :, :, 0]
        dy = deformation_matrix[:, :, :, 1]

        batch_size, height, width = dx.shape

        x_mesh, y_mesh = self.meshgrid(height, width)

        x_mesh = x_mesh.expand([batch_size, height, width])
        y_mesh = y_mesh.expand([batch_size, height, width])
        x_new = dx + x_mesh
        y_new = dy + y_mesh

        return self.interpolate(moving_image, x_new, y_new)


class UNet(torch.nn.Module):

    def __init__(self, in_channel, out_channel):
        super().__init__()
        # encode
        self.conv_encode1 = self.contracting_block(in_channels=in_channel, out_channels=32)
        self.conv_maxpool1 = torch.nn.MaxPool2d(kernel_size=2)
        self.conv_encode2 = self.contracting_block(32, 64)
        self.conv_maxpool2 = torch.nn.MaxPool2d(kernel_size=2)
        self.conv_encode3 = self.contracting_block(64, 128)
        self.conv_maxpool3 = torch.nn.MaxPool2d(kernel_size=2)
        # bottleneck
        mid_channel = 128
        self.bottleneck = torch.nn.Sequential(
            torch.nn.Conv2d(kernel_size=3, in_channels=mid_channel, out_channels=mid_channel * 2, padding=1),
            torch.nn.BatchNorm2d(mid_channel * 2),
            torch.nn.ReLU(),
            torch.nn.Conv2d(kernel_size=3, in_channels=mid_channel*2, out_channels=mid_channel, padding=1),
            torch.nn.BatchNorm2d(mid_channel),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=mid_channel, out_channels=mid_channel, kernel_size=3, stride=2, padding=1, output_padding=1),
            torch.nn.BatchNorm2d(mid_channel),
            torch.nn.ReLU(),
            )
        # decode
        self.conv_decode3 = self.expansive_block(256, 128, 64)
        self.conv_decode2 = self.expansive_block(128, 64, 32)
        self.final_layer = self.final_block(64, 32, out_channel)

    def contracting_block(self, in_channels, out_channels, kernel_size=3):
        block = torch.nn.Sequential(
            torch.nn.Conv2d(kernel_size=kernel_size, in_channels=in_channels, out_channels=out_channels, padding=1),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(),
            torch.nn.Conv2d(kernel_size=kernel_size, in_channels=out_channels, out_channels=out_channels, padding=1),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(),
        )
        return block

    def crop_and_concat(self, upsampled, bypass, crop=False):
        if crop:
            c = (bypass.size()[2] - upsampled.size()[2]) // 2
            bypass = torch.nn.functional.pad(bypass, (-c, -c, -c, -c))
        return torch.cat((upsampled, bypass), 1)

    def expansive_block(self, in_channels, mid_channel, out_channels, kernel_size=3):
        block = torch.nn.Sequential(
            torch.nn.Conv2d(kernel_size=kernel_size, in_channels=in_channels, out_channels=mid_channel, padding=1),
            torch.nn.BatchNorm2d(mid_channel),
            torch.nn.ReLU(),
            torch.nn.Conv2d(kernel_size=kernel_size, in_channels=mid_channel, out_channels=mid_channel, padding=1),
            torch.nn.BatchNorm2d(mid_channel),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=mid_channel, out_channels=out_channels, kernel_size=3, stride=2, padding=1, output_padding=1),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(),
        )
        return block

    def final_block(self, in_channels, mid_channel, out_channels, kernel_size=3):
        block = torch.nn.Sequential(
                    torch.nn.Conv2d(kernel_size=kernel_size, in_channels=in_channels, out_channels=mid_channel, padding=1),
                    torch.nn.BatchNorm2d(mid_channel),
                    torch.nn.ReLU(),
                    torch.nn.Conv2d(kernel_size=kernel_size, in_channels=mid_channel, out_channels=out_channels, padding=1),
                    torch.nn.BatchNorm2d(out_channels),
                    torch.nn.ReLU()
                )
        return block

    def forward(self, x):
        # encode
        encode_block1 = self.conv_encode1(x)
        encode_pool1 = self.conv_maxpool1(encode_block1)
        encode_block2 = self.conv_encode2(encode_pool1)
        encode_pool2 = self.conv_maxpool2(encode_block2)
        encode_block3 = self.conv_encode3(encode_pool2)
        encode_pool3 = self.conv_maxpool3(encode_block3)
        # bottleneck
        bottleneck1 = self.bottleneck(encode_pool3)
        # decode
        decode_block3 = self.crop_and_concat(bottleneck1, encode_block3)
        cat_layer2 = self.conv_decode3(decode_block3)
        decode_block2 = self.crop_and_concat(cat_layer2, encode_block2)
        cat_layer1 = self.conv_decode2(decode_block2)
        decode_block1 = self.crop_and_concat(cat_layer1, encode_block1)
        final_layer = self.final_layer(decode_block1)
        return final_layer


class VoxelMorph2D(torch.nn.Module):

    def __init__(self, in_channels=3*2):
        super().__init__()
        self.unet = UNet(in_channels, 2).cuda()
        self.spatial_transform = SpatialTransform().cuda()

    def forward(self, moving_image, fixed_image):
        x = torch.cat([moving_image, fixed_image], dim=1)
        deformation_matrix = self.unet(x).permute(0,2,3,1)
        registered_image = self.spatial_transform(moving_image.permute(0,2,3,1), deformation_matrix)
        return registered_image.permute(0,3,1,2)


class MSE:

    def loss(self, y_pred, y_true):
        return torch.mean((y_true - y_pred) ** 2)


class NCC:

    def __init__(self, win=None):
        self.win = win

    def loss(self, y_pred, y_true):
        ai = y_true  # .permute((0,2,3,1))
        bi = y_pred  # .permute((0,2,3,1))

        # get dimension of volume, # assumes ai, bi are sized [batch_size, *vol_shape, nb_feats]
        ndims = len(list(ai.size())) - 2
        assert ndims in [1, 2, 3], f'volumes should be 1 to 3 dimensions. found: {ndims}'

        # set window size
        win = [9] * ndims if self.win is None else self.win

        # compute filters
        sum_filt = torch.ones([1, 3, *win]).to('cuda')

        pad_no = int(np.floor(win[0] / 2))

        if ndims == 1:
            stride = (1)
            padding = (pad_no)
        elif ndims == 2:
            stride = (1, 1)
            padding = (pad_no, pad_no)
        else:
            stride = (1, 1, 1)
            padding = (pad_no, pad_no, pad_no)

        # get convolution function
        conv_fn = getattr(torch.nn.functional, f'conv{ndims}d')

        # compute CC squares
        a2 = ai * ai
        b2 = bi * bi
        ab = ai * bi

        a_sum = conv_fn(ai, sum_filt, stride=stride, padding=padding)
        b_sum = conv_fn(bi, sum_filt, stride=stride, padding=padding)
        a2_sum = conv_fn(a2, sum_filt, stride=stride, padding=padding)
        b2_sum = conv_fn(b2, sum_filt, stride=stride, padding=padding)
        ab_sum = conv_fn(ab, sum_filt, stride=stride, padding=padding)

        win_size = np.prod(win)
        u_a = a_sum / win_size
        u_b = b_sum / win_size

        cross = ab_sum - u_b * a_sum - u_a * b_sum + u_a * u_b * win_size
        a_var = a2_sum - 2 * u_a * a_sum + u_a * u_a * win_size
        b_var = b2_sum - 2 * u_b * b_sum + u_b * u_b * win_size

        cc = cross * cross / (a_var * b_var + 1e-5)

        return -torch.mean(cc)


def voxel_morph(*args, **kwargs):
    return VoxelMorph2D(*args, **kwargs)

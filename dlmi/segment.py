import pathlib

import imageio
import numpy as np
import PIL
import torch


class DataChestXRay(torch.utils.data.Dataset):

    def __init__(self, directory, subset, merge_hospitals=False, size_limit=None, transform=None):
        super().__init__()
        fimages = []
        fmasks = []
        if merge_hospitals or subset in ['train', 'val']:
            for fimage in sorted(pathlib.Path(directory).glob('montgomery/images/*.png')):
                fleft = fimage.parent.parent / 'masks/left' / fimage.name
                fright = fimage.parent.parent / 'masks/right' / fimage.name
                assert fleft.is_file()
                assert fright.is_file()
                fimages.append(fimage)
                fmasks.append([fleft, fright])
        if merge_hospitals or subset in ['test']:
            for fmask in sorted(pathlib.Path(directory).glob('shenzhen/masks/*.png')):
                fimage = fmask.parent.parent / 'images' / fmask.name.replace('_mask', '')
                assert fimage.is_file()
                fimages.append(fimage)
                fmasks.append([fmask])
        if size_limit is not None:
            fimages = fimages[:size_limit]
            fmasks = fmasks[:size_limit]
        nn = len(fimages)
        n = int(round(nn * 0.1))
        splits = torch.utils.data.random_split(range(nn), [nn - 2 * n, n, n])
        indices = splits[('train','val','test').index(subset)].indices
        self._fimages = [fimages[x] for x in indices]
        self._fmasks = [fmasks[x] for x in indices]
        self._transform = transform

    def __len__(self):
        return len(self._fimages)

    def __getitem__(self, index):
        fimage = self._fimages[index]
        image = imageio.imread(fimage)
        image = PIL.Image.fromarray(image.astype(np.uint8))
        image = image.convert('RGB')
        image = image.resize((256,256), PIL.Image.BILINEAR)
        image = np.asarray(image, dtype=np.float32) / 255
        image = image.transpose((2,0,1))

        fmasks = self._fmasks[index]
        mask = sum(imageio.imread(x) for x in fmasks)
        mask = PIL.Image.fromarray(mask.astype(np.uint8))
        mask = mask.resize((256,256), PIL.Image.NEAREST)
        mask = np.asarray(mask, dtype=np.uint8) / 255

        sample = {}
        sample['image'] = image
        sample['mask'] = mask
        if self._transform is not None:
            sample = self._transform(sample)
        for key, value in sample.items():
            sample[key] = torch.as_tensor(value.copy())
        return sample


class DoubleConv(torch.nn.Module):

    def __init__(self, in_channels, out_channels, mid_channels=None):
        super().__init__()
        if not mid_channels:
            mid_channels = out_channels
        self.double_conv = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1),
            torch.nn.BatchNorm2d(mid_channels),
            torch.nn.ReLU(inplace=True),
            torch.nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(inplace=True))

    def forward(self, x):
        return self.double_conv(x)


class Down(torch.nn.Module):

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.maxpool_conv = torch.nn.Sequential(
            torch.nn.MaxPool2d(2),
            DoubleConv(in_channels, out_channels))

    def forward(self, x):
        return self.maxpool_conv(x)


class OutConv(torch.nn.Module):

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.conv = torch.nn.Conv2d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        return self.conv(x)


class Up(torch.nn.Module):

    def __init__(self, in_channels, out_channels, bilinear=True):
        super().__init__()

        # if bilinear, use the normal convolutions to reduce the number of channels
        if bilinear:
            self.up = torch.nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            self.conv = DoubleConv(in_channels, out_channels, in_channels // 2)
        else:
            self.up = torch.nn.ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2)
            self.conv = DoubleConv(in_channels, out_channels)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        # input is CHW
        diffy = x2.size()[2] - x1.size()[2]
        diffx = x2.size()[3] - x1.size()[3]

        x1 = torch.nn.functional.pad(
            x1, [diffx // 2, diffx - diffx // 2,
                 diffy // 2, diffy - diffy // 2])
        # if you have padding issues, see
        # https://github.com/HaiyongJiang/U-Net-Pytorch-Unstructured-Buggy/commit/0e854509c2cea854e247a9c615f175f76fbb2e3a
        # https://github.com/xiaopeng-liao/Pytorch-UNet/commit/8ebac70e633bac59fc22bb5195e513d5832fb3bd
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class UNet(torch.nn.Module):

    def __init__(self, nchannels=3, nclasses=1, bilinear=True):
        super().__init__()
        self.nchannels = nchannels
        self.nclasses = nclasses
        self.bilinear = bilinear

        self.inc = DoubleConv(nchannels, 64)
        self.down1 = Down(64, 128)
        self.down2 = Down(128, 256)
        self.down3 = Down(256, 512)
        factor = 2 if bilinear else 1
        self.down4 = Down(512, 1024 // factor)
        self.up1 = Up(1024, 512 // factor, bilinear)
        self.up2 = Up(512, 256 // factor, bilinear)
        self.up3 = Up(256, 128 // factor, bilinear)
        self.up4 = Up(128, 64, bilinear)
        self.outc = OutConv(64, nclasses)

    def forward(self, x):
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)
        return self.outc(x)


def unet(*args, **kwargs):
    return UNet(*args, **kwargs)

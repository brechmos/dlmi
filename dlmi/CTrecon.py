"""
Functions for CNN denoising using the FFDNet.

Author: Alex Sisniega (asisniega@jhu.edu)

Including code by Matias Tassano with the following license:

Copyright (C) 2018, Matias Tassano <matias.tassano@parisdescartes.fr>

This program is free software: you can use, modify and/or
redistribute it under the terms of the GNU General Public
License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later
version. You should have received a copy of this license along
this program. If not, see <http://www.gnu.org/licenses/>.
"""

import pathlib

import imageio
import scipy.io as sio
import numpy as np
import sympy as sp
import PIL
import torch
import os
from pyronn_torch import ConeBeamProjector

from .geoTraj_conrad import circular_trajectory_3d


class DataCTsinogram(torch.utils.data.Dataset):

    def __init__(self, directory, subset, transform=None):
        super().__init__()
        directory_tmp = pathlib.Path(directory) / subset / "img"
        self._filenames = sorted(pathlib.Path(directory_tmp).glob('*.mat'))
        self._directory_label = pathlib.Path(directory) / subset / "label"
        self._subset = subset
        self._transform = transform

    def __len__(self):
        return len(self._filenames)

    def __getitem__(self, index):
        filename = self._filenames[index]
        tmp = sio.loadmat(filename)
        image = np.squeeze(tmp['l']).astype(np.float32)
        image = np.transpose(image,[1,0])
        image = image / 4.0

        # label load
        filename_label = pathlib.Path(self._directory_label) / os.path.basename(filename)
        tmp = sio.loadmat(filename_label)
        truth = np.squeeze(tmp['uUse']).astype(np.float32)
        truth = np.flip(truth,axis=1).copy()
        truth[truth<0.001] = 1e-5

        # expand dimensions
        image = np.expand_dims(image, axis=0)
        truth = np.expand_dims(truth, axis=0)

        sample = {}
        sample['image'] = image
        sample['truth'] = truth
        if self._transform is not None:
            sample = self._transform(sample)
        for key, value in sample.items():
            sample[key] = torch.as_tensor(value)
        return sample


class IntermediateDnCNN(torch.nn.Module):
    r"""Implements the middel part of the FFDNet architecture, which
    is basically a DnCNN net
    """
    def __init__(self, input_features, middle_features, num_conv_layers):
        super(IntermediateDnCNN, self).__init__()
        self.kernel_size = 3
        self.padding = 1
        self.input_features = input_features
        self.num_conv_layers = num_conv_layers
        self.middle_features = middle_features
        if self.input_features == 1:
            self.output_features = 4  # grayscale image - single channel - we split it in 4
        elif self.input_features == 4:
            self.output_features = 4  # RGB image
        else:
            raise Exception('Invalid number of input features')

        layers = []
        layers.append(torch.nn.Conv2d(in_channels=self.input_features,
                                      out_channels=self.middle_features,
                                      kernel_size=self.kernel_size,
                                      padding=self.padding,
                                      bias=False))
        layers.append(torch.nn.ReLU(inplace=True))
        for _ in range(self.num_conv_layers-2):
            layers.append(torch.nn.Conv2d(in_channels=self.middle_features,
                                          out_channels=self.middle_features,
                                          kernel_size=self.kernel_size,
                                          padding=self.padding,
                                          bias=False))
            layers.append(torch.nn.BatchNorm2d(self.middle_features))
            layers.append(torch.nn.ReLU(inplace=True))
        layers.append(torch.nn.Conv2d(in_channels=self.middle_features,
                                      out_channels=self.output_features,
                                      kernel_size=self.kernel_size,
                                      padding=self.padding,
                                      bias=False))
        self.itermediate_dncnn = torch.nn.Sequential(*layers)

    def forward(self, x):
        out = self.itermediate_dncnn(x)
        return out


class SqueezeLayer(torch.nn.Module):
    def __init__(self, dim):
        super(SqueezeLayer, self).__init__()
        self.dim = dim
        
    def forward(self, x):
        return x.squeeze(self.dim)

class UnSqueezeLayer(torch.nn.Module):
    def __init__(self, dim):
        super(UnSqueezeLayer, self).__init__()
        self.dim = dim
        
    def forward(self, x):
        return x.unsqueeze(self.dim)


class CTRecNet(torch.nn.Module):
    r"""Implements the FFDNet architecture
    """
    def __init__(self):
        super(CTRecNet, self).__init__()

        # hardcoded parameters
        self.num_feature_maps = 32
        self.num_conv_layers = 10
        self.downsampled_channels = 1
        self.output_features = 4

        # recon parameters - Hardcoded for the time being
        self.pm = circular_trajectory_3d(180, 2*np.pi, 800, 500, np.array((1, 256)), np.array((0.8, 0.8)))

        self.intermediate_dncnn = IntermediateDnCNN(
                input_features=self.downsampled_channels,
                middle_features=self.num_feature_maps,
                num_conv_layers=self.num_conv_layers)
        
        self.intermediate_dncnn2 = IntermediateDnCNN(
                input_features=4,
                middle_features=16,
                num_conv_layers=10)
        
        self.projector = ConeBeamProjector_AS(
                        (1, 256, 256),
                        (0.5, 0.5, 0.5),
                        (-63.5, -63.5 , 0),
                        (180, 1, 256),
                        (0.8, 0.8),
                        (0, 0),
                        self.pm)
        self.outBN = torch.nn.BatchNorm2d(4)
        self.outLayer = torch.nn.Conv2d(in_channels=4,
                                      out_channels=1,
                                      kernel_size=1,
                                      padding=0,
                                      bias=False)
        
        self.unsqueeze = UnSqueezeLayer(3)
        self.squeeze = SqueezeLayer(2)

    def forward(self, x):
        h_dncnn = self.intermediate_dncnn(x)
        CT_in = self.unsqueeze(h_dncnn)
        CT_tmp = self.projector(CT_in)
        CT_tmp = self.squeeze(CT_tmp)
        h_dncnn = self.intermediate_dncnn2(CT_tmp)
        h_dncnn = self.outBN(h_dncnn)
        CT_out = self.outLayer(h_dncnn)
        return CT_out, CT_in


class ConeBeamProjector_AS(torch.nn.Module):
    def __init__(self, volume_shape, volume_spacing, volume_origin, projection_shape, projection_spacing, projection_origin, projection_matrices):
        super(ConeBeamProjector_AS, self).__init__()

        self._volume_shape = volume_shape
        self._volume_origin = volume_origin
        self._volume_spacing = volume_spacing
        self._projection_shape = projection_shape
        self._projection_matrices_numpy = projection_matrices
        self._projection_spacing = projection_spacing
        self._projection_origin = projection_origin
        # create projector object
        self.projector = ConeBeamProjector(
                        self._volume_shape,
                        self._volume_spacing,
                        self._volume_origin,
                        self._projection_shape,
                        self._projection_spacing,
                        self._projection_origin,
                        self._projection_matrices_numpy)
        
    def forward(self, x):
        dtype = x.data.type()
        CT_tmp = torch.zeros((x.shape[0], x.shape[1], self._volume_shape[0], self._volume_shape[1], self._volume_shape[2]), requires_grad=x.requires_grad).type(dtype)
        for nInstance in range(x.shape[0]):
            for nChannel in range(x.shape[1]):
                tmp = torch.squeeze(torch.squeeze(x[nInstance,nChannel,:,:,:],0),0)
                input_prj = self.projector.new_projection_tensor(requires_grad=x.requires_grad)
                input_prj += tmp
                tmp2 = self.projector.project_backward(input_prj, use_texture=True)
                CT_tmp[nInstance,nChannel,:,:,:] = tmp2*100000
                del input_prj
        return torch.autograd.Variable(CT_tmp, requires_grad=x.requires_grad)

    def backward(self, x):
        dtype = x.data.type()
        prj_tmp = torch.zeros((x.shape[0], x.shape[1], self._volume_shape[0], self._volume_shape[1], self._volume_shape[2]), requires_grad=x.requires_grad).type(dtype)
        for nInstance in range(x.shape[0]):
            for nChannel in range(x.shape[1]):
                tmp = torch.squeeze(torch.squeeze(x[nInstance,nChannel,:,:,:],0),0)
                prj_tmp[nInstance,nChannel,:,:,:] = self.projector.project_forward(tmp, use_texture=True)/100000
        print(prj_tmp.shape)
        return torch.autograd.Variable(prj_tmp, requires_grad=x.requires_grad)


def weights_init_kaiming(lyr):
    r"""Initializes weights of the model according to the "He" initialization
    method described in "Delving deep into rectifiers: Surpassing human-level
    performance on ImageNet classification" - He, K. et al. (2015), using a
    normal distribution.
    This function is to be called by the torch.nn.Module.apply() method,
    which applies weights_init_kaiming() to every layer of the model.
    """
    classname = lyr.__class__.__name__
    if classname.find('Conv') != -1:
        torch.nn.init.kaiming_normal_(lyr.weight.data, a=0, mode='fan_in')
    elif classname.find('Linear') != -1:
        torch.nn.init.kaiming_normal_(lyr.weight.data, a=0, mode='fan_in')
    elif classname.find('BatchNorm') != -1:
        lyr.weight.data.normal_(mean=0, std=np.sqrt(2./9./64.)).\
            clamp_(-0.025, 0.025)
        torch.nn.init.constant_(lyr.bias.data, 0.0)


# Aux functions to perform ramp filtering
def fbpRampFilter(y, fact, hamming, SDD, SAD, u0, v0, PixSize, **kwargs):

    (nu, nv, nb) = y.shape

    if len(SDD) == 1:
        DS = np.tile(SDD, nb)
        A = np.tile(SAD, nb)
        u0 = np.tile(u0, nb)
        v0 = np.tile(v0, nb)
    else:
        DS = SDD
        A = SAD
        u0 = u0
        v0 = v0
    
    # Pixel sizes in mm
    uPixelSize = PixSize[0]
    vPixelSize = PixSize[1]

    # Filter specification in u
    NU = int(2**np.ceil(np.log2(nu)))
    filter = np.zeros((2*NU,1))
    arg1 = np.pi * fact * np.arange(0,nu) # s coordinates where the filter is calculated
    h1 = hamming * vfunc(arg1) + 0.5 * (1-hamming) * (vfunc(np.pi + arg1) + vfunc(np.pi - arg1))
    arg2 = np.pi * fact * np.arange(-nu+1, 0)
    h2 = hamming * vfunc(arg2) + 0.5 * (1-hamming) * (vfunc(np.pi + arg2) + vfunc(np.pi - arg2))
    filter[0:nu] = np.expand_dims(h1, axis=1)
    filter[(2*NU-nu+1):(2*NU)] = np.expand_dims(h2, axis=1)
    filter = 2 * (fact**2) / (2*uPixelSize*2) * filter
    filter = np.fft.fft(filter, axis=0)

    y_filt = np.zeros((nu,nv,nb),'float32')

    for b in np.arange(0, nb):

        p_mat = uPixelSize * np.matmul(np.ones((nu,1),'float32'), (np.expand_dims(np.arange(0,nv), axis=0) - v0[b] - nv/2))
        eta_mat = vPixelSize * np.matmul((np.expand_dims(np.arange(0,nu), axis=1) - u0[b] - nu/2), np.ones((1,nv),'float32'))
    
        lt = y[:,:,b]
    
        lt = lt * DS[b] / np.sqrt(DS[b]**2+p_mat**2+eta_mat**2) * (A[b]/DS[b])
    
        pad = np.zeros((2*NU-nu,lt.shape[1]))
        tmp = np.concatenate((lt, pad), axis=0)
        tmp = np.fft.fft(tmp, axis=0)
        tmp = np.tile(filter,(1,lt.shape[1],)) * tmp
        tmp = np.real(np.fft.ifft(tmp,axis=0))
        y_filt[:,:,b] = tmp[0:nu,:] # un-pad

    return y_filt


# aux filter function
def vfunc(s):
    i = np.argwhere(np.abs(s) > 0.0001)
    v = np.zeros(s.shape)
    ss = s[i]
    v[i] = np.sin(ss)/ss + (np.cos(ss)-1) / (ss**2)
    v[np.abs(s) <= 0.0001] = 0.5
    return v
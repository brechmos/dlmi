import numpy as np


class GaussianNoise:

    def __init__(self, mean, std):
        self._mean = mean
        self._std = std

    def __call__(self, sample):
        for key, value in sample.items():
            if key in ['image', 'fixed', 'moving']:
                mean = np.random.uniform(0, self._mean)
                std = np.random.uniform(0, self._std)
                sample[key] = np.clip(value + np.random.normal(mean, std, value.shape).astype(value.dtype), *np.percentile(value, (0, 100)))
        return sample


class RandomHorizontalFlip:

    def __call__(self, sample):
        if np.random.rand() < 0.5:
            return sample
        for key, value in sample.items():
            if key == 'boxes':
                image = sample['image']
                xdim = image.shape[image.ndim - 1]
                for i, (xmin, ymin, xmax, ymax) in enumerate(value):
                    sample[key][i][:] = xdim - xmax, ymin, xdim - xmin, ymax
            elif key in ['image', 'mask', 'fixed', 'moving']:
                sample[key][:] = np.flip(value, value.ndim - 1)
        return sample


class RandomVerticalFlip:

    def __call__(self, sample):
        if np.random.rand() < 0.5:
            return sample
        for key, value in sample.items():
            if key == 'boxes':
                image = sample['image']
                ydim = image.shape[image.ndim - 2]
                for i, (xmin, ymin, xmax, ymax) in enumerate(value):
                    sample[key][i][:] = xmin, ydim - ymax, xmax, ydim - ymin
            elif key in ['image', 'mask', 'fixed', 'moving']:
                sample[key][:] = np.flip(value, value.ndim - 2)
        return sample


def collate(batch):
    return {x: [y[x] for y in batch] for x in batch[0].keys()}

from matplotlib import patches


def box(ax, xmin, ymin, xmax, ymax, **kwargs):
    kwargs.setdefault('fc', 'none')
    ax.add_patch(patches.Rectangle((xmin, ymin), xmax - xmin + 1, ymax - ymin + 1, **kwargs))


def grid(cols=1, rows=1, size=3, pad=0, image=False, **kwargs):
    from matplotlib import pyplot as plt
    from mpl_toolkits.axes_grid1 import Grid, ImageGrid
    layout = cols, rows
    figsize = [size * layout[x] + pad * (layout[x] + 1) for x in range(2)]
    fig = plt.figure(figsize=figsize)
    padr = [pad / figsize[x] for x in range(2)]
    rect = padr[0], padr[1], 1 - padr[0] * 2, 1 - padr[1] * 2
    grid = ImageGrid if image else Grid
    axs = grid(fig, rect, layout[::-1], axes_pad=pad, **kwargs)
    if image:
        for ax in axs.axes_all:
            ax.set_xticks([])
            ax.set_yticks([])
    return fig, axs

import setuptools


setuptools.setup(
    name='dlmi',
    version='1.0.0',
    author='Ali Uneri',
    author_email='ali.uneri@jhu.edu',
    classifiers=[
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3'],
    packages=setuptools.find_packages(),
    install_requires=[
        'imageio',
        'matplotlib',
        'numpy',
        'pillow',
        'pyronn_torch',
        'torch',
        'torchvision'],
    python_requires='>=3.6')
